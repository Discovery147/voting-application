package com.sizonenko.vote.page;

/**
 * The interface Page.
 */
public interface Page {
    /**
     * The constant INDEX_PAGE.
     */
    String INDEX_PAGE = "index";
    /**
     * The constant VOTE_PAGE.
     */
    String VOTE_PAGE = "vote";
    /**
     * The constant ERROR_PAGE.
     */
    String ERROR_PAGE = "error_page";
}

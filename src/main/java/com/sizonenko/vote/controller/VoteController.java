package com.sizonenko.vote.controller;

import com.sizonenko.vote.model.Topic;
import com.sizonenko.vote.page.Page;
import com.sizonenko.vote.receiver.BaseReceiver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * The type Vote controller.
 */
@Controller
@RequestMapping(path = "/api")
public class VoteController {

    private final BaseReceiver receiver;

    /**
     * Instantiates a new Main controller.
     *
     * @param receiver the receiver
     */
    @Autowired
    public VoteController(BaseReceiver receiver) {
        this.receiver = receiver;
    }

    /**
     * Home page.
     *
     * @return the string
     */
    @GetMapping("/")
    public String home() {
        return Page.INDEX_PAGE;
    }

    /**
     * Gets topic.
     *
     * @param code    the code
     * @param session the session
     * @param model   the model
     * @return the topic | error page
     */
    @GetMapping(path = "/topic")
    public String getTopic(@RequestParam String code, HttpSession session, Model model) {
        return receiver.findTopicByUrl(code, session.getId(), model);
    }

    /**
     * Add a new topic
     *
     * @param topic the topic
     * @return the topic's code | DATA_INVALID
     */
    @PostMapping(path = "/topic")
    public @ResponseBody
    ResponseEntity addTopic(@RequestBody Topic topic) {
        return ResponseEntity.ok(receiver.addTopic(topic));
    }

    /**
     * Vote.
     *
     * @param topic   the topic
     * @param session the session
     * @return the json: true | false
     */
    @PostMapping(path = "/vote", produces="application/json")
    public @ResponseBody
    ResponseEntity addVote(@RequestBody Topic topic, HttpSession session) {
        return ResponseEntity.ok(receiver.addVote(topic, session.getId()));
    }
}

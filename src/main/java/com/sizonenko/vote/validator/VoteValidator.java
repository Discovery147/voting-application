package com.sizonenko.vote.validator;

import com.sizonenko.vote.model.Option;
import com.sizonenko.vote.model.Topic;
import com.sizonenko.vote.repository.OptionRepository;
import com.sizonenko.vote.repository.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.Optional;

@Component
public class VoteValidator {

    private final TopicRepository topicRepository;
    private final OptionRepository optionRepository;

    @Autowired
    public VoteValidator(TopicRepository topicRepository, OptionRepository optionRepository) {
        this.topicRepository = topicRepository;
        this.optionRepository = optionRepository;
    }

    public boolean validateAddTopic(Topic topic) {
        if (topic == null) {
            return false;
        }
        if(topic.getTitle() == null || topic.getTitle().length() == 0){
            return false;
        }
        if (topic.getOptions() == null || topic.getOptions().size() < 2) {
            return false;
        }
        return true;
    }

    public boolean validateFindTopic(String code) {
        return code != null;
    }

    public boolean validateAddVote(Topic topic) {
        if(topic == null || topic.getOptions() == null){
            return false;
        }
        if(topicRepository.findById(topic.getTopicId()).equals(Optional.empty())){
            return false;
        }
        Iterator<Option> optionsIter = topic.getOptions().iterator();
        if(optionRepository.findById(optionsIter.next().getOptionId()).equals(Optional.empty())){
            return false;
        }
        return true;
    }
}

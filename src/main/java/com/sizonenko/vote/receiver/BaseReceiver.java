package com.sizonenko.vote.receiver;

import com.sizonenko.vote.model.Topic;
import com.sizonenko.vote.page.Page;
import com.sizonenko.vote.validator.VoteValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

@Service
public class BaseReceiver {
    private final TopicReceiver topicReceiver;
    private final OptionReceiver optionReceiver;
    private final MemberReceiver memberReceiver;
    private final VoteValidator validator;

    @Autowired
    public BaseReceiver(TopicReceiver topicReceiver, OptionReceiver optionReceiver, MemberReceiver memberReceiver, VoteValidator validator) {
        this.topicReceiver = topicReceiver;
        this.optionReceiver = optionReceiver;
        this.memberReceiver = memberReceiver;
        this.validator = validator;
    }

    public String addTopic(Topic topic) {
        if (validator.validateAddTopic(topic)) {
            return topicReceiver.addTopic(topic);
        }
        return "DATA_INVALID";
    }

    public String findTopicByUrl(String code, String sessionId, Model model) {
        if (validator.validateFindTopic(code)) {
            return topicReceiver.findTopicByUrl(code, model, sessionId);
        }
        return Page.ERROR_PAGE;
    }

    public boolean addVote(Topic topic, String sessionId) {
        if (validator.validateAddVote(topic)) {
            optionReceiver.addVote(topic);
            memberReceiver.addMember(topic, sessionId);
            return true;
        }
        return false;
    }
}


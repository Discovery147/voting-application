package com.sizonenko.vote.receiver;

import com.sizonenko.vote.model.Member;
import com.sizonenko.vote.model.Topic;
import com.sizonenko.vote.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MemberReceiver {
    private final MemberRepository repository;

    @Autowired
    public MemberReceiver(MemberRepository repository) {
        this.repository = repository;
    }

    void addMember(Topic topic, String sessionId) {
        repository.save(new Member(topic, sessionId));
    }
}

package com.sizonenko.vote.receiver;

import com.sizonenko.vote.model.Topic;
import com.sizonenko.vote.page.Page;
import com.sizonenko.vote.repository.TopicRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.Objects;
import java.util.Optional;

@Service
public class TopicReceiver {
    private static final byte KEY_LENGTH = 6;
    private final TopicRepository repository;

    @Autowired
    public TopicReceiver(TopicRepository repository) {
        this.repository = repository;
    }

    String addTopic(Topic topic) {
        topic.getOptions().forEach(option -> option.setTopic(topic));
        generateKey(topic);
        while (repository.existsByUrl(topic.getUrl())) {
            generateKey(topic);
        }
        repository.save(topic);
        return topic.getUrl();
    }

    private void generateKey(Topic topic) {
        topic.setUrl(RandomStringUtils.randomAlphanumeric(KEY_LENGTH).toUpperCase());
    }

    String findTopicByUrl(String code, Model model, String sessionId) {
        Topic topic = repository.findByUrl(code);
        if (topic != null) {
            boolean voted = topic.getMembers().stream().anyMatch(member -> Objects.equals(member.getSessionId(), sessionId));
            model.addAttribute("topic", topic);
            model.addAttribute("voted", voted);
            return Page.VOTE_PAGE;
        }
        return Page.ERROR_PAGE;
    }

    Optional<Topic> findTopicById(long topicId) {
        return repository.findById(topicId);
    }
}

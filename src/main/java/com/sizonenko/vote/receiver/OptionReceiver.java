package com.sizonenko.vote.receiver;

import com.sizonenko.vote.model.Option;
import com.sizonenko.vote.model.Topic;
import com.sizonenko.vote.repository.OptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;

@Service
public class OptionReceiver {
    private final OptionRepository repository;

    @Autowired
    public OptionReceiver(OptionRepository repository) {
        this.repository = repository;
    }

    void addVote(Topic topic) {
        Iterator<Option> optionsIter = topic.getOptions().iterator();
        repository.addVote(optionsIter.next().getOptionId());
    }
}

package com.sizonenko.vote.repository;

import com.sizonenko.vote.model.Option;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface OptionRepository extends CrudRepository<Option, Long> {
    @Transactional
    @Modifying
    @Query("UPDATE Option o SET o.count = (o.count+1) WHERE o.optionId=:optionId")
    void addVote(@Param("optionId") Long optionId);
}

package com.sizonenko.vote.repository;

import com.sizonenko.vote.model.Topic;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TopicRepository extends CrudRepository<Topic, Long> {
    @Query("SELECT CASE WHEN COUNT(t) > 0 THEN true ELSE false END FROM Topic t WHERE t.url = :url")
    boolean existsByUrl(@Param("url") String url);

    @Query("SELECT t FROM Topic t WHERE t.url=:url")
    Topic findByUrl(@Param("url") String url);
}
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Sizonenko Valeriy">
        <title>Creative - Start Voting</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
              integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
              rel='stylesheet' type='text/css'>
        <link href='https:fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'
              rel='stylesheet' type='text/css'>
        <link href="../../vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
        <link href="../../css/index.min.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <header class="masthead text-center text-white d-flex">
            <div class="container my-auto">
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <h1 class="text-uppercase">
                            <strong>Here you can create themes for voting</strong>
                        </h1>
                        <hr>
                    </div>
                    <div class="col-lg-8 mx-auto">
                        <p class="text-faded mb-5">After creating a vote, a link is displayed, which you can send to your
                            friends!</p>
                        <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Create vote</a>
                    </div>
                </div>
            </div>
        </header>
        <section class="bg-primary" id="about">
            <div class="container">
                <div class="wrapper">
                    <form class="form-addtopic">
                        <h3 class="form-addtopic-heading">Create VOTE</h3>
                        <hr class="colorgraph">
                        <input type="text" class="form-control" id="title" placeholder="Theme" required autofocus=""/>
                        <hr class="colorgraph">
                        <div id="beginOptions"></div>
                        <button class="btn btn-primary btn-block" style="margin-top: 10px" onclick="addOption()" type="button">
                            Add option
                        </button>
                        <button class="btn btn-primary btn-block" style="margin-top: 10px" onclick="deleteLatestOption()"
                                type="button">Delete latest option
                        </button>
                        <hr class="colorgraph">
                        <button class="btn btn-lg btn-primary btn-block" name="Submit" value="Create" type="button"
                                onclick="createTopic()">Create
                        </button>
                    </form>
                </div>
                <div class="modal fade bs-example-modal-lg" id="getUrlModal" tabindex="-1" role="dialog"
                     aria-labelledby="myLargeModalLabel">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-body">
                                <h2>Voting has begun! Invite your friends via the link!</h2>
                                <div id="containerIntro">
                                    <h4>Your link: </h4>
                                    <p id="url"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Bootstrap core JavaScript -->
        <script src="../../vendor/jquery/jquery.min.js"></script>
        <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Plugin JavaScript for Slow Scrolling -->
        <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Custom scripts -->
        <script src="../../js/index.min.js"></script>
    </body>
</html>

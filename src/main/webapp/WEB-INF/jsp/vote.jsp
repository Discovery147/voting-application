<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Sizonenko Valeriy">
    <title>Vote now!</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <link href="../../css/vote.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <div class="container">
      <div class="row main">
        <div class="main-login main-center">
          <div style="text-align: center;"><h4>${topic.title}</h4></div><br>
          <c:forEach items="${topic.options}" var="option">
            <c:choose>
              <c:when test="${voted}">
                <button type="button" class="btn btn-primary btn-lg btn-block login-button" disabled> <span class="badge">${option.count}</span> ${option.name}</button>
              </c:when>
              <c:when test="${not voted}">
                <button type="button" class="btn btn-primary btn-lg btn-block login-button" onclick="vote(${option.optionId}, ${topic.topicId})">${option.name}</button>
              </c:when>
            </c:choose>
          </c:forEach>
          <br>
          Total votes: ${topic.members.size()}
          <a href="http://localhost:8080"><button type="button" class="btn btn-primary btn-lg btn-block login-button">Create new topic</button></a>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Custom scripts -->
    <script src="../../js/vote.js"></script>
  </body>
</html>

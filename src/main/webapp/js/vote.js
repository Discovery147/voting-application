function vote(optionId, topicId){
    var data = {"topicId":topicId, "options": []};
    data.options.push({"optionId": optionId});
    data = JSON.stringify(data);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: data,
        url: "/api/vote",
        success: function (msg) {
            alert("Thanks for your vote!");
            location.reload();
        }
    });
}
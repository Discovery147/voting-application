# Voting application

######Allows you to create votes and share them with friends.

1. Clone the project
2. Execute SQL file 'vote_db.sql'
3. Configure the 'application.properties' file
4. Run: mvn spring-boot:run